USE hospital;


/*
Persona(codigo_nombre,  nombre, apellido )
CK = PK= {codigo_nombre}
FK = {}
*/
CREATE TABLE IF NOT EXISTS persona(  
    codigo_nombre int NOT NULL, /* Tipo de dato correcto? */     
    nombre varchar(45) NOT NULL,  
    apellido varchar(45) NOT NULL,   
    PRIMARY KEY (codigo_nombre)  
); 

/*
Paciente(dni, fecha_nacimiento, sexo, codigo_nombre)  
PK = CK = {dni}
FK = {codigo_nombre}
*/
CREATE TABLE IF NOT EXISTS paciente(  
    dni int NOT NULL,  
    fecha_nacimiento DATE NOT NULL,  
    sexo varchar(40) NOT NULL,   
    codigo_nombre int,
    PRIMARY KEY (dni),  
    FOREIGN KEY(codigo_nombre)
    REFERENCES persona(codigo_nombre)
);  



/*
Medico(nro_matricula, dni, foto, codigo_nombre, fecha_inicio_vacaciones, cantidad_guardias, fecha_fin_vacaciones, cuit/cuil, fecha_ingreso)
PK= {nro_matricula}
CK = {dni, nro_matricula, cuit/cuil}
FK = {codigo_nombre}
*/
CREATE TABLE IF NOT EXISTS medico(
    nro_matricula int NOT NULL,
    _dni int NOT NULL,
    foto varchar(128), /* hacemos de cuenta que la foto es un buffer de 128 bytes... */
    codigo_nombre int,
    fecha_inicio_vacaciones DATE NOT NULL,
    cantidad_guardias int NOT NULL,
    fecha_fin_vacaciones DATE NOT NULL,
    cuil_cuit int NOT NULL,
    fecha_ingreso DATE NOT NULL,
    PRIMARY KEY (nro_matricula),
    FOREIGN KEY (codigo_nombre) REFERENCES persona(codigo_nombre),
    CHECK (cantidad_guardias <= 90)  
);

/*
Especialidad(cod_especialidad, descripción)
PK = CK = { cod_especialidad}
*/
CREATE TABLE IF NOT EXISTS especialidad(
    cod_especialidad int NOT NULL,
    descripcion varchar(128),
    PRIMARY KEY (cod_especialidad)
);

/*
SeEspecializa(nro_matricula, cod_especialidad, disponible)
PK = CK = { ( nro_matricula, cod_especialidad ) }
FK = { nro_matricula, cod_especialidad }
*/
CREATE TABLE IF NOT EXISTS seEspecializa(
    nro_matricula int NOT NULL,
    cod_especialidad int NOT NULL,
    disponible varchar(10) NOT NULL,
    PRIMARY KEY (nro_matricula, cod_especialidad),
    FOREIGN KEY (nro_matricula)    REFERENCES medico(nro_matricula),
    FOREIGN KEY (cod_especialidad) REFERENCES especialidad(cod_especialidad)
);

/*
Guardia(codigo_guardia, fecha_guardia, turno, nro_matricula, cod_especialidad)
PK = CK = { codigo_guardia }
FK = { (nro_matricula, cod_especialidad) }
*/
CREATE TABLE IF NOT EXISTS guardia(
    codigo_guardia int NOT NULL,
    fecha_guardia DATE NOT NULL,
    turno varchar(16) NOT NULL,
    nro_matricula int,
    cod_especialidad int,
    PRIMARY KEY (codigo_guardia),
    FOREIGN KEY (nro_matricula, cod_especialidad) REFERENCES seEspecializa(nro_matricula, cod_especialidad)
);

/*
Ronda(codigo_ronda, turno, dia)
CK = PK = {codigo_ronda}
*/
CREATE TABLE IF NOT EXISTS ronda(
    codigo_ronda int NOT NULL,
    turno varchar(16) NOT NULL,
    dia DATE NOT NULL,
    PRIMARY KEY (codigo_ronda)
);

/*
Recorrido(codigo_recorrido, codigo_ronda, nro_matricula)
PK = CK = { codigo_recorrido }
FK = {nro_matricula, codigo_ronda}
*/
CREATE TABLE IF NOT EXISTS recorrido(
    codigo_recorrido int NOT NULL,
    codigo_ronda int NOT NULL,
    nro_matricula int NOT NULL,
    PRIMARY KEY (codigo_recorrido),
    FOREIGN KEY (nro_matricula) REFERENCES medico(nro_matricula),
    FOREIGN KEY (codigo_ronda)  REFERENCES ronda(codigo_ronda)
);

/*
Internación(dni, fecha_inicio, fecha_fin, nro_matricula)
PK = { (dni, fecha_inicio) }
CK = {(dni, fecha_inicio), (dni, fecha_fin)} 
FK = { dni , nro_matricula}
*/
CREATE TABLE IF NOT EXISTS internacion(
    dni int NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    nro_matricula int NOT NULL,
    PRIMARY KEY (dni, fecha_inicio),
    FOREIGN KEY (dni)           REFERENCES paciente(dni),
    FOREIGN KEY (nro_matricula) REFERENCES medico(nro_matricula)
);

/*
Habitacion(nro_habitacion, piso, sector, orientación)
CK = PK= {nro_habitacion}
*/
CREATE TABLE IF NOT EXISTS habitacion(
    nro_habitacion int NOT NULL,
    piso int NOT NULL,
    sector varchar(32) NOT NULL,
    orientación varchar(32) NOT NULL,
    PRIMARY KEY (nro_habitacion)
);

/*
Cama (nro_habitacion, nro_cama, ocupada)
CK = PK= {(nro_habitacion, nro_cama)}
FK= {nro_habitacion}
*/
CREATE TABLE IF NOT EXISTS cama(
    nro_habitacion int NOT NULL,
    nro_cama int NOT NULL,
    ocupada varchar(32) NOT NULL,
    PRIMARY KEY (nro_habitacion, nro_cama),
    FOREIGN KEY (nro_habitacion) REFERENCES habitacion(nro_habitacion)
);

/*
Utiliza( fecha_inicio, nro_cama, fecha_asignacion,)
PK = CK = { (fecha_inicio, nro_cama, fecha_asignacion) }
FK = { fecha_fin, nro_cama }
*/
CREATE TABLE IF NOT EXISTS utiliza(
    fecha_inicio DATE,
    dni int NOT NULL,
    nro_cama int NOT NULL,
    nro_habitacion int NOT NULL,
    fecha_asignacion DATE NOT NULL,
    PRIMARY KEY (fecha_inicio, fecha_asignacion, nro_cama),
    FOREIGN KEY (dni, fecha_inicio) REFERENCES internacion(dni, fecha_inicio),
    FOREIGN KEY (nro_habitacion, nro_cama)     REFERENCES cama(nro_habitacion, nro_cama)
);

/*
Ronda (codigo_ronda, turno, dia)
CK = PK = {codigo_ronda}
*/
CREATE TABLE IF NOT EXISTS ronda(
    codigo_ronda int NOT NULL,
    turno varchar(32) NOT NULL,
    dia DATE NOT NULL,
    PRIMARY KEY (codigo_ronda),
    CHECK(turno = 'mañana' OR turno = 'tarde' OR turno = 'noche')
);

/*
Recorre(codigo_ronda, nro_habitacion)
PK = CK = { (codigo_ronda, nro_habitacion) }
FK = { codigo_ronda, nro_habitacion }
*/
CREATE TABLE IF NOT EXISTS recorre(
    codigo_ronda int NOT NULL,
    nro_habitacion int NOT NULL,
    PRIMARY KEY (codigo_ronda, nro_habitacion),
    FOREIGN KEY (codigo_ronda) REFERENCES ronda(codigo_ronda),
    FOREIGN KEY (nro_habitacion) REFERENCES habitacion(nro_habitacion)
);

/*
EsRecorrida(fecha_inicio, codigo_recorrido)
PK = CK = { (fecha_inicio, codigo_recorrido) }
FK = { fecha_inicio, codigo_recorrido }
*/
CREATE TABLE IF NOT EXISTS esRecorrida(
    fecha_inicio DATE NOT NULL,
    codigo_recorrido int NOT NULL,
    dni int NOT NULL,
    PRIMARY KEY (fecha_inicio, codigo_recorrido),
    FOREIGN KEY (dni, fecha_inicio) REFERENCES internacion(dni, fecha_inicio),
    FOREIGN KEY (codigo_recorrido) REFERENCES recorrido(codigo_recorrido)
);

/*
Comentario(codigo_comentario, fecha, turno, texto, fecha_inicio, codigo_recorrido)
CK = PK= {codigo_comentario}
FK = { (fecha_inicio, codigo_recorrido) }
*/
CREATE TABLE IF NOT EXISTS comentario(
    codigo_comentario int NOT NULL,
    fecha DATE NOT NULL,
    turno varchar(32) NOT NULL,
    texto varchar(128) NOT NULL,
    fecha_inicio DATE NOT NULL,
    codigo_recorrido int NOT NULL,
    PRIMARY KEY (codigo_comentario),
    FOREIGN KEY (fecha_inicio, codigo_recorrido) REFERENCES esRecorrida(fecha_inicio, codigo_recorrido)
);

/* 
Listado con la cantidad de camas disponibles de cada sector.
*//*
CREATE PROCEDURE ListarTodasLasCamasDeCadaSector()
BEGIN
	SELECT habitacion.sector , COUNT(habitacion.nro_cama)
    FROM habitacion, cama 
    WHERE cama.nro_habitacion = habitacion.nro_habitacion 
          AND cama.ocupada = 'disponible'; 
END

/*
El listado de detalle de esas
camas (requerimiento del área de internaciones)
*//*
CREATE PROCEDURE ContarCamasDeCadaSector()
BEGIN
	SELECT *  
    FROM cama, habitacion 
    WHERE cama.nro_habitacion = habitacion.nro_habitacion 
          AND cama.ocupada = 'disponible'; 
END

/*
 Listado de los comentarios de las visitas médicas a un paciente en una cierta internación
(requerimiento del área de seguimiento médico)
*//*
CREATE PROCEDURE ListarComentariosDePaciente()
BEGIN
	SELECT comentario(*)  
    FROM comentario, internacion, recorrido, paciente
    WHERE comentario.fecha_inicio = internacion.fecha_inicio AND comentario.codigo_recorrido = recorrido.codigo_recorrido
          AND internacion.dni = paciente.dni; 
END

/*
Auditoría sobre los usuarios que hacen cambios a datos que afectan el proceso de
asignación de guardias (requerimiento del área de asignación de guardias)
*//*
CREATE PROCEDURE AuditarUsuarios()
BEGIN
	SELECT *  FROM ; 
END

/*
Listado con la asignación posible del personal para cubrir guardias del mes (requerimiento
del área de asignación de guardias): el proceso de asignación de guardias se efectúa una
vez por mes y el sector quiere que el sistema le emita un listado con una asignación
posible del personal para cubrir cada guardia teniendo en cuenta todas las limitaciones
(vacaciones, máximo de guardias que aceptaría el médico, etc.) Además de estas
limitaciones se debe tener en cuenta que un médico no puede estar de guardia en dos días
seguidos.
*//*
CREATE PROCEDURE MostrarPersonalDisponibleGuardia()
BEGIN
	SELECT *  FROM ; 
END


/* carga de datos */
INSERT IGNORE INTO
    persona(codigo_nombre,  nombre, apellido ) 
VALUES (20,'Hernan','Stroll'), 
       (21,'Hernan','Stroll'), 
       (59,'Javier','Alonso'),
       (77,'Leticia','Martinez'),
       (55,'Candela','Victorel'),
       (11,'Eduardo','Diez'),
       (33,'Victoria','Paniagua'),
       (269987, 'Hernando', 'Alonso'),
       (895787, 'Pepe', 'Pepon'),
       (459231, 'Pepe', 'Argento'),
       (459231, 'Moni', 'Argento'),
       (154866, 'Maria', 'Fuseneco');


INSERT IGNORE INTO
    medico(nro_matricula, dni, foto, codigo_nombre, fecha_inicio_vacaciones, cantidad_guardias, fecha_fin_vacaciones, cuil_cuit, fecha_ingreso) 
VALUES (35488, 39335263, 'd15',20,'2019-03-21',3,'2019-04-07','11506789','2018-09-11'),
       (38498, 39875263, 'ard5',59,'2008-10-03',7,'2008-12-10','64574789','2006-01-10'),
       (35456, 16235263, 'asd1',77,'2005-07-17',22,'2005-10-05','17536722','1999-02-14'),
       (45896, 45957562, 'q5ed',55,'2017-09-21',50,'2018-04-7','11506789','2000-12-23'),
       (45635, 44269588, '5575dar1',21,'1999-02-10',77,'2000-01-01','33759789','1990-10-10'),
       (36589, 11256638, 'ad52d2',33,'2015-05-10',57,'2015-05-20','15879565','2004-02-16');

INSERT IGNORE INTO
    paciente(dni, fecha_nacimiento, sexo, codigo_nombre) 
VALUES (38695720, '2001-01-29','M',269987),
       (48624580, '2007-04-30','M',895787),
       (59535670, '1990-07-05','H',459231),
       (56323564, '2010-06-17','H',157896),
       (26869658, '1980-10-15','M',154866);

INSERT IGNORE INTO
    internacion(dni, fecha_inicio, fecha_fin, nro_matricula)
VALUES (38695720,'2020-06-07','2020-07-07',35488),
       (48624580,'2017-09-13','2018-12-23',38498),
       (59535670,'2000-05-28','2000-10-15',35456),
       (56323564,'2006-11-11','2007-02-20',45896),
       (26869658,'2010-02-30','2012-10-24',45635);

INSERT IGNORE INTO
    cama(nro_habitacion, nro_cama, ocupada)
VALUES (15,1,TRUE),
       (15,2,TRUE),
       (15,4,FALSE),
       (10,1,TRUE),
       (10,2,FALSE),
       (10,6,TRUE),
       (7,2,FALSE),
       (7,3,FALSE),
       (7,4,FALSE),
       (7,5,FALSE);

INSERT IGNORE INTO
    habitacion(nro_habitacion, piso, sector, orientación)
VALUES (15,2,3,'norte'),
       (10,2,5,'sur'),
       (1,1,1,'norte'),
       (7,1,10,'sur'),
       (6,1,3,'norte'),
       (12,5,3,'sur'),
       (8,2,10,'norte')

